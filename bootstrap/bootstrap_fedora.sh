#!/bin/bash
#
# Build a minimalist image capable of buildah, podman, skopeo, curl, jq,
# date and test.

set -e
set -x

dnf install -y --setopt=install_weak_deps=False \
	skopeo \
	jq \
	git-core \
	diffutils \
	buildah \
	podman \
	runc \
	fuse-overlayfs \
	procps-ng \
	xz

cat > /etc/containers/registries.conf <<EOF
# This is a system-wide configuration file used to
# keep track of registries for various container backends.
# It adheres to TOML format and does not support recursive
# lists of registries.

# The default location for this configuration file is /etc/containers/registries.conf.

# The only valid categories are: 'registries.search', 'registries.insecure',
# and 'registries.block'.

[registries.search]
registries = ['docker.io', 'registry.fedoraproject.org', 'quay.io', 'registry.centos.org']

# If you need to access insecure registries, add the registry's fully-qualified name.
# An insecure registry is one that does not have a valid SSL certificate or only does HTTP.
[registries.insecure]
registries = []


# If you need to block pull access from a registry, uncomment the section below
# and add the registries fully-qualified name.
#
# Docker only
[registries.block]
registries = []
EOF

cat > $buildmnt/etc/containers/containers.conf <<EOF
[engine]
cgroup_manager = "cgroupfs"
EOF

cat > $buildmnt/etc/containers/policy.json <<EOF
{
    "default": [
        {
            "type": "insecureAcceptAnything"
        }
    ],
    "transports":
        {
            "docker-daemon":
                {
                    "": [{"type":"insecureAcceptAnything"}]
                }
        }
}
EOF

cat > $buildmnt/etc/containers/storage.conf <<EOF
# This file is is the configuration file for all tools
# that use the containers/storage library.
# See man 5 containers-storage.conf for more information
# The "container storage" table contains all of the server options.
[storage]

# Default Storage Driver
driver = "vfs"

# Temporary storage location
runroot = "/var/run/containers/storage"

# Primary Read/Write location of container storage
graphroot = "/var/lib/containers/storage"
EOF

chmod 644 /etc/containers/containers.conf
sed -i -e 's|^#mount_program|mount_program|g' \
	-e '/additionalimage.*/a "/var/lib/shared",' \
	-e 's|^mountopt[[:space:]]*=.*$|mountopt = "nodev,fsync=0"|g' \
	/etc/containers/storage.conf
mkdir -p /var/lib/shared/overlay-images \
	/var/lib/shared/overlay-layers \
	/var/lib/shared/vfs-images \
	/var/lib/shared/vfs-layers
touch /var/lib/shared/overlay-images/images.lock
touch /var/lib/shared/overlay-layers/layers.lock
touch /var/lib/shared/vfs-images/images.lock
touch /var/lib/shared/vfs-layers/layers.lock

# The cbuild script is used by all container-build and qemu-build
# templates.
# The templates pull the matching cbuild with their particular sha,
# so we provide one placeholder here instead.
mkdir -p $buildmnt/app
cat > $buildmnt/app/cbuild <<EOF
#!/bin/bash

echo "This is the cbuild script placeholder. If you see this, the template did not fetch the latest cbuild script correctly. This may be a bug with ci-templates."
exit 1
EOF
chmod +x $buildmnt/app/cbuild
