#
# THIS FILE IS GENERATED, DO NOT EDIT
#

.fdo.ubuntu:
  variables:
    FDO_DISTRIBUTION_NAME: "ubuntu"
    # project 5761 is freedesktop/ci-templates
    FDO_CBUILD: https://gitlab.freedesktop.org/api/v4/projects/5761/packages/generic/cbuild/sha256-12cae8e43a1c3814d03cefd985f9bf46e4106eafa369eda85953c83435bdef95/cbuild
    DEBIAN_FRONTEND: noninteractive

.pull.cbuild@ubuntu:
  script: &pull-cbuild
  - echo $FDO_CBUILD
  - echo $CI_API_V4_URL
  - |
    if [[ "$FDO_CBUILD" =~ ^$CI_API_V4_URL ]]
    then
      # the given URL is on the instance (gitlab.fd.o) API endpoint, use our token
      # so private projects can also retrieve cbuild in their namespace
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" \
           --retry 4 --fail --retry-all-errors --retry-delay 60 \
           -L -o /app/cbuild \
           $FDO_CBUILD
    else
      curl -L --retry 4 --fail --retry-all-errors --retry-delay 60 \
           -o /app/cbuild $FDO_CBUILD
    fi
  - chmod +x /app/cbuild

.fdo.container-build-common@ubuntu:
  extends: .fdo.ubuntu
  stage: build
  script:
  - *pull-cbuild
  - /app/cbuild --debug build-container ubuntu $FDO_DISTRIBUTION_VERSION $FDO_DISTRIBUTION_TAG
  artifacts:
    paths:
      - container-build-report.xml
    reports:
      junit: container-build-report.xml

###
# Checks for a pre-existing ubuntu container image and builds it if
# it does not yet exist.
#
# If an image with the same version or suffix exists in the upstream project's registry,
# the image is copied into this project's registry.
# If no such image exists, the image is built and pushed to the local registry.
#
# The architecture is native to the runner, use the `tags:` field in the job to
# select a runner that is not the default x86_64.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-ubuntu-image:
#       extends: .fdo.container-build@ubuntu
#       variables:
#          FDO_DISTRIBUTION_PACKAGES: 'curl wget gcc valgrind'
#          FDO_DISTRIBUTION_VERSION: '20.04'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#
#     my-ubuntu-arm-image:
#       extends: .fdo.container-build@ubuntu
#       variables:
#          FDO_DISTRIBUTION_PACKAGES: 'curl wget gcc valgrind'
#          FDO_DISTRIBUTION_VERSION: '20.04'
#          FDO_DISTRIBUTION_TAG: '2020-03-20-arm'
#       tags:
#         - aarch64
#
#
# **Reserved by this template:**
#
# - ``image:`` do not override
# - ``script:`` do not override
#
# **Environment variables set by default:**
#
# - ``DEBIAN_FRONTEND=noninteractive``
#
# **Variables:**
#
# .. attribute:: FDO_DISTRIBUTION_VERSION
#
#       **This variable is required**
#
#       The ubuntu version to build, e.g. '20.04', '22.04'
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
# .. attribute:: FDO_UPSTREAM_REPO
#
#       The GitLab project path to the upstream project
#
# .. attribute:: FDO_REPO_SUFFIX
#
#       The repository name suffix to use, see below.
#
# .. attribute:: FDO_DISTRIBUTION_PACKAGES
#
#       Packages to install as a space-separated single string, e.g. "curl wget".
#       These packages must be available in the default distribution repositories; use
#       :attr:`FDO_DISTRIBUTION_EXEC` followed by the distribution-specific
#       command(s) to enable additional repositories and/or install additional
#       packages.
#
# .. attribute:: FDO_DISTRIBUTION_EXEC
#
#       An executable run after the installation of the :attr:`FDO_DISTRIBUTION_PACKAGES`
#
# .. attribute:: FDO_DISTRIBUTION_ENTRYPOINT
#
#       The path to the binary that should be used as an entrypoint
#
# .. attribute:: FDO_DISTRIBUTION_WORKINGDIR
#
#       The path that will be used as the default working directory (default: /app)
#
# .. attribute:: FDO_FORCE_REBUILD
#
#       If set, the image will be built even if it exists in the registry already
#
# .. attribute:: FDO_BASE_IMAGE
#
#       By default, the base image to start with is
#       ``ubuntu:$FDO_DISTRIBUTION_VERSION``
#       and all dependencies are installed on top of that base image. If
#       ``FDO_BASE_IMAGE`` is given, it references a different base image to start with.
#       This image usually requires the full registry path, e.g.
#       ``registry.freedesktop.org/projectGroup/projectName/repo_suffix:tag-name``
#
# .. attribute:: FDO_EXPIRES_AFTER
#
#       If set, enables an expiration time on the image to
#       aid the garbage collector in deciding when an image can be removed. This
#       should be set for temporary images that are not intended to be kept
#       forever. Allowed values are e.g. ``1h`` (one hour), ``2d`` (two days) or
#       ``3w`` (three weeks).
#
# .. attribute:: FDO_CACHE_DIR
#
#       If set, the given directory is mounted as ``/cache``
#       when ``FDO_DISTRIBUTION_EXEC`` is run. This can allow for passing of
#       cache values between build jobs (if run on the same runner).  You should
#       not usually need to set this, it defaults to ``/cache`` from the host
#       and thus enables cache sharing by default.
#
# .. attribute:: FDO_USER
#
#       If set, the given unix username is used when running containers based on
#       this image instead of root. If the username is not created by the
#       :attr:`FDO_DISTRIBUTION_EXEC` script, it will be created automatically.
#       Note that $HOME will be set to ``/home/$FDO_USER``, if the user is
#       created manually by :attr:`FDO_DISTRIBUTION_EXEC` script it is important
#       to ensure that directory is created as well and writable by the user.
#
# The resulting image will be pushed to the local registry.
#
# If :attr:`FDO_REPO_SUFFIX` was specified, the image path is
# ``$CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG``.
# Use the ``.fdo.suffixed-image@ubuntu`` template to access or use this image.
#
# If :attr:`FDO_REPO_SUFFIX` was **not** specified, the image path is
# ``$CI_REGISTRY_IMAGE/ubuntu/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG``.
# Use the ``.fdo.distribution-image@ubuntu`` template to access or use this image.
#
.fdo.container-build@ubuntu:
  extends: .fdo.container-build-common@ubuntu
  image: quay.io/freedesktop.org/ci-templates:container-build-base-2024-06-05.0

###
# Alias to ``.fdo.container-build@ubuntu``.
#
# This template is deprecated, use .fdo.container-build@ubuntu instead.
# The architecture is inferred by the runner selected by the tags on the job (if any).
.fdo.container-build@ubuntu@x86_64:
  extends: .fdo.container-build@ubuntu


###
# Checks for a pre-existing ubuntu container image for the
# ``aarch64`` processor architecture and builds it if it does not yet exist.
#
# This template requires runners with the ``aarch64`` tag.
#
# See ``.fdo.container-build@ubuntu`` for details.
#
# This template is deprecated, use .fdo.container-build@ubuntu instead.
# The architecture is inferred by the runner selected by the tags on the job (if any).
.fdo.container-build@ubuntu@aarch64:
  extends: .fdo.container-build@ubuntu
  tags:
    - aarch64


###
# ubuntu template that pulls the ubuntu image from the
# registry based on ``FDO_DISTRIBUTION_VERSION`` and ``FDO_DISTRIBUTION_TAG``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@ubuntu``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-ubuntu-test:
#       extends: .fdo.distribution-image@ubuntu
#       variables:
#          FDO_DISTRIBUTION_VERSION: '20.04'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - meson builddir
#         - ninja -C builddir test
#
# **Variables:**
#
# .. attribute:: FDO_DISTRIBUTION_VERSION
#
#       **This variable is required**
#
#       The ubuntu version to build, e.g. '20.04', '22.04'
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "ubuntu"
#
# .. note:: If you used ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.suffixed-image@ubuntu`` instead.
.fdo.distribution-image@ubuntu:
  extends: .fdo.ubuntu
  image: $CI_REGISTRY_IMAGE/ubuntu/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/ubuntu/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG

###
# ubuntu template that pulls the ubuntu image from the
# registry based on ``FDO_REPO_SUFFIX``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@ubuntu``.
#
# This template sets ``image:`` to the generated image. You may override this.
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-ubuntu-test:
#       extends: .fdo.distribution-image@ubuntu
#       variables:
#          FDO_REPO_SUFFIX: 'some/path'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - meson builddir
#         - ninja -C builddir test
#
#
# **Variables:**
#
# .. attribute:: FDO_REPO_SUFFIX
#
#       **This variable is required**
#
#       The repository name suffix.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "ubuntu"
#
#
# Variables provided by this template should be considered read-only.
#
# .. note:: If you did not use ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.distribution-image@ubuntu`` instead.
.fdo.suffixed-image@ubuntu:
  extends: .fdo.ubuntu
  image: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG

###
# boot2container ubuntu template that pulls the ubuntu image from the
# registry based on ``FDO_DISTRIBUTION_VERSION`` and ``FDO_DISTRIBUTION_TAG``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@ubuntu``.
#
# This template sets ``image:`` to a boot2container capable image. You may
# override this if your image is also capable of running it.
#
# This template will also set the following environment variables:
#
# - ``B2C_IMAGE``: will default to ``$FDO_DISTRIBUTION_IMAGE`` if not set
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-ubuntu-test:
#       extends: .fdo.b2c-image@ubuntu
#       variables:
#          FDO_DISTRIBUTION_VERSION: '20.04'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - B2C_COMMAND="meson builddir" /app/boot2container
#         - B2C_COMMAND="ninja -C builddir test" /app/boot2container
#
# **Variables:**
#
# .. attribute:: FDO_DISTRIBUTION_VERSION
#
#       **This variable is required**
#
#       The ubuntu version to build, e.g. '20.04', '22.04'
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "ubuntu"
#
# .. note:: If you used ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.b2c-suffixed-image@ubuntu`` instead.
.fdo.b2c-image@ubuntu:
  extends:
    - .fdo.ubuntu
  image: quay.io/freedesktop.org/ci-templates:qemu-base-2024-06-05.0
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/ubuntu/$FDO_DISTRIBUTION_VERSION:$FDO_DISTRIBUTION_TAG
    B2C_IMAGE: $FDO_DISTRIBUTION_IMAGE


###
# boot2container ubuntu template that pulls the ubuntu image from the
# registry based on ``FDO_REPO_SUFFIX``.
# This template must be provided the same variable values as supplied in
# ``.fdo.container-build@ubuntu``.
#
# This template sets ``image:`` to a boot2container capable image. You may
# override this if your image is also capable of running it.
#
# This template will also set the following environment variables:
#
# - ``B2C_IMAGE``: will default to ``$FDO_DISTRIBUTION_IMAGE`` if not set
#
# **Example:**
#
# .. code-block:: yaml
#
#     my-ubuntu-test:
#       extends: .fdo.b2c-suffixed-image@ubuntu
#       variables:
#          FDO_REPO_SUFFIX: 'some/path'
#          FDO_DISTRIBUTION_TAG: '2020-03-20'
#       script:
#         - B2C_COMMAND="meson builddir" /app/boot2container
#         - B2C_COMMAND="ninja -C builddir test" /app/boot2container
#
#
# **Variables:**
#
# .. attribute:: FDO_REPO_SUFFIX
#
#       **This variable is required**
#
#       The repository name suffix.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_TAG
#
#       **This variable is required**
#
#       String to identify the image in the registry.
#
#       The value supplied must be the same as supplied in
#       ``.fdo.container-build@ubuntu``.
#
# .. attribute:: FDO_DISTRIBUTION_IMAGE
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Path to the registry image
#
# .. attribute:: FDO_DISTRIBUTION_NAME
#
#       **This variable is set by this template and should be treated as read-only**
#
#       Set to the string "ubuntu"
#
# .. note:: If you did not use ``FDO_REPO_SUFFIX`` when building the container, use
#           ``.fdo.b2c-image@ubuntu`` instead.
.fdo.b2c-suffixed-image@ubuntu:
  extends: .fdo.b2c-image@ubuntu
  variables:
    FDO_DISTRIBUTION_IMAGE: $CI_REGISTRY_IMAGE/$FDO_REPO_SUFFIX:$FDO_DISTRIBUTION_TAG
    B2C_IMAGE: $FDO_DISTRIBUTION_IMAGE
